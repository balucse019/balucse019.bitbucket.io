function tQuery(str){
    
    this.arr = [];
    
    this.arr[0] = str;
    
    if(this instanceof tQuery)
        return this;
    else 
    return new tQuery(str);
    
    }
    
    tQuery.prototype.split = function(seperator){
    
    this.arr = this.arr.map(ele => {
    return ele.split(seperator)
    }).reduce(function(acc,curr){
        return acc.concat(curr);
    },[]);
    
    return this;
    }
    
    tQuery.prototype.reverse = function(){
    
    this.arr = this.arr.map(ele => ele.split('').reverse().join('')
    );
    return this;
    }
    
    tQuery.prototype.join = function(delim){
      this.arr = new Array(this.arr.join(delim));
      return this;
    }
    
    tQuery.prototype.get = function(){
        return this.arr;
    }
    

    