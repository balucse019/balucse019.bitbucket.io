describe("disemvowel",function(){
    it("should remove vowles",function(){
        expect(disemvowel("balakrishna")).toEqual("blkrshn");
    });
    it("should not change strings with novowles",function(){
        expect(disemvowel("mkrst")).toEqual("mkrst");
    });
    it("should remove capital vowels also",function(){
        expect(disemvowel("BalA")).toEqual("Bl");
    });
})