describe("tquery",function(){

    var tq;

    beforeEach(function(){
        tq = tQuery("12+23|45+67");
    })

    it("should split the string to arr",function(){
        expect(tq.split('|').get()).toEqual(['12+23','45+67']);
    });

    it("should reverse the elements of array",function(){
        expect(tq.split('|').reverse().get()).toEqual(['32+21','76+54']);
    });

    it("should reverse the elements of array. Not the array",function(){
        expect(tq.split('|').reverse().get()).not.toEqual(['45+67','12+23']);
    });

    it("should join the elements of array",function(){
        expect(tq.split('|').reverse().split('+').join('-').get()).toEqual(['32-21-76-54']);
    });

    it("should returned object itself is tquery obj",function(){
        expect(tq.split('+') instanceof tQuery).toBeTruthy();
    });

    

});

describe("filter",function(){
    it("should return even numbers",function(){
        expect(evenfilter([2,1,5,6,7])).toEqual([2,6]);
    });
    it("should be equal to empty array when empty array is passed",function(){
        expect(evenfilter([])).toEqual([]);
    });
    
    it("single odd should return empty",function(){
        expect(evenfilter([1])).toEqual([]);
    });

});

describe("find sum",function(){
    it("should work with natural numbers",function(){
        expect(findSum([1,2,3,5,4])).toEqual(15);
    });
    it("should work for both natural and negative numbers",function(){
        expect(findSum([1,2,3,-5,4])).toEqual(5);
    });
});